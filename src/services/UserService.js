import axios from 'axios'

const apiClient = axios.create({  
	baseURL: `http://radiant-plains-67953.herokuapp.com/api/`,
	//withCredentials: false,
	headers: {
		Accept: 'application/json', 'Content-Type': 'application/json'
	}
})

apiClient.interceptors.response.use(
	function (response) {
		return response;
	},
	function (error) {
		console.error('API Error, ' + error);
	return Promise.reject(error);
	}
);

const RESOURCE_NAME = 'users';

export default {
	createUser(user)
	{
		return apiClient.post(RESOURCE_NAME, user);
	},
	getUser(id, header){
        return apiClient.get(`${RESOURCE_NAME}/${id}`, header);
	},
	verifyUser(login)
	{
		return apiClient.get(`${RESOURCE_NAME}/${login}/verify`);
	},
	login(user){
		return apiClient.post(RESOURCE_NAME+"/login", user);
	},
	post(user){
		return apiClient.post(`${RESOURCE_NAME}`, user)
	},
	update(id, user, header){
		return apiClient.put(`${RESOURCE_NAME}/${id}`, JSON.stringify(user), header);
	},
	updatePassword(id, password, header){
		return apiClient.put(`${RESOURCE_NAME}/${id}/password`, password, header);
	}
}
