import axios from 'axios'

const apiClient = axios.create({  
	baseURL: `http://radiant-plains-67953.herokuapp.com/api`,
	headers: {
		Accept: 'application/json', 'Content-Type': 'application/json'
	}
})

apiClient.interceptors.response.use(
	function (response) {
		return response;
	},
	function (error) {
		console.error('API Error, ' + error);
	return Promise.reject(error);
	}
);

const RESOURCE_NAME = 'films';

export default {
	getFilms(page) {
		return apiClient.get(`${RESOURCE_NAME}?page=${page}`)
	},
	getFilm(id) {
		return apiClient.get(`${RESOURCE_NAME}/${id}`)
	},
	getActors(id) {
		return apiClient.get(`${RESOURCE_NAME}/${id}/actors`)
	},
	getFilmsSearchResults(keyword) {
		return apiClient.get(`${RESOURCE_NAME}?keyword=${keyword}`)
	},
	deleteFilm(id, header){
		return apiClient.delete(`${RESOURCE_NAME}/${id}`, header);
	},
	postFilm(film, header){
		return apiClient.post(RESOURCE_NAME, JSON.stringify(film), header);
	},
	postCritic(id, critic, header){
		return apiClient.post(`${RESOURCE_NAME}/${id}/critics`, JSON.stringify(critic),header );
	},
	putCritic(filmId, critic,criticId, header){
		return apiClient.put(`${RESOURCE_NAME}/${filmId}/critics/${criticId}`, JSON.stringify(critic),header );
	}
}
