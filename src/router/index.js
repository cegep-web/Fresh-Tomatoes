import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import FilmDetailsView from '../views/FilmDetailsView.vue'
import ConnectionView from '../views/ConnectionView'
import LogoutView from '../views/LogoutView'
import AdminView from '../views/AdminView.vue'
import UserView from '../views/UserView.vue'
import ChangePasswordView from '../views/ChangePasswordView'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    props:true
  },
  {
    path: '/login',
    name: 'login',
    component: ConnectionView,
    props: (route) => ({
      history: route.params
    })
  },
  {
    path: '/logout',
    name: 'logout',
    component: LogoutView,
    props: (route) => ({
      history: route.params
    })
  },
  {
    path: '/details/:id',
    name: 'details',
    component: FilmDetailsView,
    props: (route) => ({
      id: route.params.id,
      film: route.params.film,
  })
  },
  {
    path: '/admin',
    name: 'admin',
    component: AdminView,
  },
  {
    path: '/user',
    name: 'user',
    component: UserView,
  },
  {
    path: '/user/password',
    name: 'password',
    component: ChangePasswordView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  mode: 'history',
  history: createWebHistory(),
  routes
})

export default router
