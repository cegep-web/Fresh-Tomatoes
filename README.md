# Travail pratique: Vue.js

## 1 Objectif

Développer une application frontend en Vue.js qui utilise une API développée en PHP avec le Framework Laravel et donc qui permettra de gérer **un site de critiques de films**. Cette application Web permettra :
- d’ajouter et de supprimer des films (seulement si admin)
- d’ajouter ou de modifier une critique de film (si connecté)
- de créer ou de modifier un compte utilisateur
- de se connecter pour avoir le droit de gérer le contenu du site et avoir accès à des pages sécurisées
- de gérer trois types d’utilisateurs différents : administrateurs, membres et visiteurs

## 2 Conditions de réalisation du travail

Votre site devra permettre les situations décrites plus bas – le mot « page » est utilisé ici de façon générale et
ne signifie pas un fichier.

### Fonctionnalités à développer :

- Une page d’accueil affichant un mot de bienvenue aux utilisateurs ainsi qu’un aperçu des **trois derniers films** ajoutés :
    - titre du film
    - image de l’affiche (petit format)
    - cote des membres (selon les critiques)
    - les 100 premiers caractères du synopsis suivi de 3 points de suspension
    - bouton permettant de consulter le détail du film 
- Une barre dans le haut, toujours présente, permettant la recherche de films et affichant un résumé des films trouvés :
    - titre du film
    - image de l’affiche
    - cote des membres (selon les critiques)
    - les 100 premiers caractères du synopsis suivi de 3 points de suspension
    - son classement (‘R’, ‘13+’, etc.)
    - sa durée
    - bouton permettant de consulter le détail du film

La recherche doit permettre de trouver un film en recherchant les mots inscrits **dans le titre du film ou son synopsis**. Les films sont affichés par **ordre décroissant** de leur année de parution.

Dans le cas où la recherche est infructueuse (on ne trouve aucun film répondant au(x) critère(s) de recherche), vous devez afficher un message indiquant qu’il n’y a aucun résultat qui correspond à cette recherche.

Dans le cas où un ou des films répondent au(x) critère(s) de recherche, vous devez afficher un message indiquant combien de films ont été trouvés et présenter les résultats de la recherche avec un maximum de 20 films par page.

- Une page affichant le détail d’un film. Cette page contient 3 sections :
La première section affiche le détail du film :
    - Titre du film
    - Affiche du film
    - Vote des membres avec nombre de votes total
    - Le synopsis du film complet
    - Classement du film
    - Durée
    - L’année de parution
    - Les acteurs

Le calcul de la cote d’un film n’est pas affiché (les étoiles sont grises) tant qu’il n’a pas reçu un **minimum de 5 critiques**.

La deuxième section n’est visible **que si un membre est connecté**. Celle-ci contient un formulaire permettant à un membre d’ajouter une critique. Ce formulaire doit contenir les champs suivants :

-
    - Un vote (nombre d’étoiles sur cinq). Il doit être possible de sélectionner une demiétoile.
    - Un commentaire facultatif (255 caractères maximum). 
    - Un bouton « Ajouter » ou « Modifier » selon le contexte

Si un membre a déjà fait une critique alors vous devez préremplir les champs du formulaire avec leurs valeurs actuelles et permettre au membre de modifier sa critique (une modification à votre API sera alors nécessaire).

Le texte du bouton doit être différent selon qu’il s’agit d’un ajout ou d’une modification. Vous devez faire apparaître un message indiquant si l’ajout ou la modification a bien fonctionné.

La troisième section contient les commentaires des autres membres. Les commentaires sont affichés en **ordre décroissant** de date. Vous devez afficher **deux commentaires par ligne** (comme sur le site de [RottenTomatoes](https://www.rottentomatoes.com/) mais sans les bulles). Pour chaque membre ayant fait un commentaire, on affiche :

    - Le commentaire
    - Le nom du membre
    - La date du commentaire en format long (ex. : 1 janvier 2020).

**Attention, le commentaire du membre connecté ne doit pas apparaître dans cette section.**

- Lorsqu’un administrateur est connecté au site, et qu’on est sur la page de détails d’un film, on doit afficher un bouton permettant la suppression du film.
- Une page permettant à un utilisateur de se créer ou modifier un compte. Le formulaire doit contenir les champs suivants :
    - Nom d’utilisateur (50 caractères maximum). Doit être unique.
        - Le nom d’utilisateur doit contenir un minimum de 3 caractères.
        - Lorsqu’un utilisateur inscrit un nom d’utilisateur et change de champ, vous devez à partir du troisième caractère saisi vérifier en asynchrone si ce nom d’utilisateur existe déjà dans la base de données.
    - Mot de passe (50 caractères maximum).
    - Confirmation du mot de passe (50 caractères maximum).
    - Courriel (50 caractères maximum).
    - Nom et prénom (50 caractères maximum chacun).
    - Un bouton « S’inscrire » ou « Modifier » selon le contexte.

Un message d’erreur doit être affiché si la création du compte n’a pas fonctionné.

Un message de confirmation doit être affiché si la modification du compte a fonctionné.
- Une page permettant à un membre de se connecter. Le formulaire doit contenir les champs suivants :
    - Nom d’utilisateur (50 caractères maximum).
    - Mot de passe (50 caractères maximum).
    - Un bouton « Se connecter ».

Un message d’erreur doit être affiché si la connexion n’a pas fonctionné.

Une confirmation visuelle lorsque la connexion a bien fonctionné.

Une fois connecté, l’utilisateur est redirigé à la page d’où il venait avant la connexion.

- On doit pouvoir se connecter et se déconnecter au besoin de façon conviviale. La page de connexion ainsi que la page de création d’un compte doivent être accessibles à partir d’un lien situé dans l’en-tête du site.
Lors que l’utilisateur est connecté, on affiche le nom de l’utilisateur dans l’en-tête, on affiche un lien « **Profil** » permettant à un utilisateur de modifier son profil et on remplace le lien « **Se connecter** » par un lien « **Se déconnecter** ».
- Vous devez construire une section administrative qui sera accessible seulement lorsque l’utilisateur est connecté en tant qu’administrateur. Le menu de l’administrateur sera donc différent du menu standard.
Cette section devra contenir les options supplémentaires suivantes:
    - Gérer le contenu :
        - Cette page doit permettre **d’ajouter** et **supprimer** des films. La page d’ajout d’un film doit contenir un formulaire avec les champs suivants :
        - Titre du film
        - Image de l’affiche du film (ce champ doit permettre de télécharger l’image dans le sous-dossier « films » du dossier « images » du serveur). Le nom du fichier doit être unique et remplacer si l’administrateur télécharge une autre image. Les images doivent être redimensionnées physiquement avant  l’enregistrement sur le serveur.
        - Année de production (4 caractères maximum et ne doit pas être antérieur à l’année courante)
        - Durée en minutes
        - Le classement (G, 13+,16+,18+). **Cette liste doit provenir de la base de données**.
        - Les acteurs
        - Le synopsis du film
        - Un bouton « Ajouter »

Un message d’erreur doit être affiché si la création d’un film n’a pas fonctionné.

Un message de confirmation doit être affiché si l’ajout d’un film a fonctionné.

Cette page n’est accessible que lorsqu’un administrateur est connecté.

## 3 Directives générales
- Le titre (title) de chaque page doit être complet et pertinent.
- Toutes les données envoyées par formulaire doivent être **validées**.
- Dans les cas où on ajoute, modifie ou supprime un enregistrement dans une table, vous devez faire apparaître un **message de confirmation** indiquant si l’opération a réussi ou non.
- Vous devez compléter les styles pour **la présentation graphique**.
- Respectez la structure de Vue.js.